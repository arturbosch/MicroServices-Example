package de.artismarti.communicationservice;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author artur
 */
@RestController
public class NewsController {

	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public ResponseEntity<String> ping() {
		try {
			Thread.sleep(1000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok("NewsService at your service.");
	}

}
