Mircroservice example project using spring boot and CompletableFutures.

## Usage:
- mvn clean package on the parent pom
- start all four services
- call localhost:4440/ping for synchronous communication
- call localhost:4440/aping for asynchronous communication
- i've added 1 sec delay for each service to simulate processing!