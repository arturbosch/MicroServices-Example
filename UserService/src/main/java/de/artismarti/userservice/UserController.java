package de.artismarti.userservice;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author artur
 */
@RestController
public class UserController {

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String ping() {
		try {
			Thread.sleep(1000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "UserService at your service.";
	}

}
