package de.artismarti.profileservice;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author artur
 */
@RestController
public class ProfileController {

	@RequestMapping(value = "/profiles", method = RequestMethod.GET)
	public String ping() {
		try {
			Thread.sleep(1000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "ProfileService at your service.";
	}
	
}
