package de.artismarti.controllingservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @author artur
 */
@RestController
public class ConnectorController {

	private Logger logger = LoggerFactory.getLogger(ConnectorController.class);

	@RequestMapping(value = "/ping", method = RequestMethod.GET)
	public String ping() {

		RestTemplate rest = new RestTemplate();

		long average = 0L;

		for (int i = 1; i <= 10; i++) {

			long start = System.nanoTime();

			String news = rest.getForObject("http://localhost:4441/news", String.class);
			String user = rest.getForObject("http://localhost:4442/users", String.class);
			String profile = rest.getForObject("http://localhost:4443/profiles", String.class);

			logger.info("Round " + i + ": " + news + " " + user + " " + profile);
			average = (System.nanoTime() - start) / 1_000_000;
		}

		logger.info("Average time is: " + average + " msecs.");

		return "finished";
	}

	@RequestMapping(value = "/aping", method = RequestMethod.GET)
	public String asyncPing() {

		RestTemplate rest = new RestTemplate();

		long average = 0L;

		for (int i = 1; i <= 10; i++) {

			long start = System.nanoTime();

			Future<Object> future = CompletableFuture.supplyAsync(() ->
					rest.getForObject("http://localhost:4441/news", String.class))
					.thenCombine(CompletableFuture.supplyAsync(
							() -> rest.getForObject("http://localhost:4442/users", String.class)),
							(news, user) -> (news + " " + user + " "))
					.thenCombine(CompletableFuture.supplyAsync(
							() -> rest.getForObject("http://localhost:4443/profiles", String.class)),
							(newsAndUser, profile) -> (newsAndUser + " " + profile));

			try {
				logger.info("Round " + i + ": " + future.get());
			} catch (InterruptedException | ExecutionException e) {
				logger.info("Round " + i + ": Exception - " + e);
			}
			average = (System.nanoTime() - start) / 1_000_000;
		}

		logger.info("Average time is: " + average + " msecs.");

		return "finished async";
	}
}
